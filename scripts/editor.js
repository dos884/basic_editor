function getAllEvents(element) {
    var result = [];
    for (var key in element) {
        if (key.indexOf('on') === 0) {
            result.push(key.slice(2));
        }
    }
    // return result.join(' ');
    //why the fuck would u join that?!
    return result;
}

function print(msg){
    console.log(msg);
    $("#mini_console").text(msg);
}

$("#canvas_container").click(function () {

    console.log("hi");
});

quickDelegate = function(event, target) {
    // console.log(target);
    var eventCopy = document.createEvent("MouseEvents");
    eventCopy.initMouseEvent(event.type, event.bubbles, event.cancelable, event.view, event.detail,
        event.pageX || event.layerX, event.pageY || event.layerY, event.clientX, event.clientY, event.ctrlKey, event.altKey,
        event.shiftKey, event.metaKey, event.button, event.relatedTarget);
    target[0].dispatchEvent(eventCopy);
    // ... and in webkit I could just dispath the same event without copying it. eh.
};

function new_node_at(){}

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

$(document).ready(function() {

    var $dragging = null;
    var adding_node = false;
    var graph_container = $('#graph_container');
    var mode = null;
    var node_unique_id = 0;
    var mouse_guard=false;

    //get all events
    var jq_canvas = $('#canvas_container');
    // var all_evts = getAllEvents(jq_canvas[0]);
    // var all_evts =["click", "dblclick", "mousedown", "mouseup", "mouseover", "mouseout", "contextmenu", "mousenter", "mouseleave"];
    var all_evts =["click", "dblclick", "mousedown", "mouseup", "contextmenu"];
    console.log(all_evts);
    all_evts.forEach(function (el, i) {
        console.log("found: "+el);
        jq_canvas.on(el,function (e) {
            $(e.target).hide();
            var under_elem = $(document.elementFromPoint(e.pageX, e.pageY));
            // console.log(under_elem);
            quickDelegate(e,under_elem);
            $(e.target).show();
        });
    });

    graph_container.on('mousedown',function(e) {
        if (mode === "node" && !adding_node) {
            adding_node=true;
            var newChild=(
                "<div class=\"editor_container\" style=\"left:{0}px; top: {1}px;\"> "+
                "<div class=\"move_button\"></div>"
                // "<div id=\"d{2}\" class=\"editable_div\"></div></div>"
            ).format(e.pageX,e.pageY,node_unique_id);
            console.log(newChild);


            // console.log(newChild);
            graph_container.append(newChild);
//             var node = $('#'+node_unique_id);
//             node.offset({top: e.pageY,
//                 left: e.pageX
// //                    top: e.pageY,
// //                    left: e.pageX
//                 });
            node_unique_id++;
        }
    });

    $('#btn_add_edge').click(function () {
        print("Add edge mode");
        mode="edge";
    });
    $("#btn_add_node").click(function () {
        print("Add node mode");
        mode="node";
    });
    $("#btn_delete").click(function () {
        print("del mode");
        mode="delete";
    });
    $("#btn_auto_del").click(function (e) {
        print(e.target.checked);
    });

    $(document).on("mousemove", function(e) {
        if ($dragging) {

            $dragging.offset({
                top: e.pageY,
                left: e.pageX
//                    top: e.pageY,
//                    left: e.pageX
            });
        }
    });
     // $(document).on("mouseup",function (e) {
     //
     // });

    // $(".editor_container").click(function(){
    //     console.log($(this));
    //     console.log($(this).is(".editor_container"));
    // });

    graph_container.on("mousedown",".editor_container",function (e) {

        //find the closest container (dont drag tinyMCE editor!)
        $dragging = $(e.target).closest('.editor_container');
        // editor_change_state(false);
    });

    $(document).on("mouseup", function (e) {

        $dragging = null;
        adding_node=false;
        // editor_change_state(true);
    });
});