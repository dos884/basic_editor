const ADJACENCY_LIST_KEY = "a", NAME_KEY = "n", DESCRIPTION_KEY = "d", CONTENT_KEY = "c", DELIM = "#_#",
    ID_PREFIX_FOR_CUSTOM_EDGES = "ce";

var UIModeEnum = Object.freeze({NEUTRAL: 0, ADD_EDGE: 1, DELETE: 2});
var edgeCounter = 0;

var EdgeTypesEnum = Object.freeze({
    GENERIC: 0, ANS: 1, ELAB: 2, PREREQ: 3, LEADS: 4, SIMILAR: 5,
    properties: {
        0:{typeName:"generic", elementColor: '#e7e7e7'},
        1:{typeName:"answer", elementColor: '#217419'},
        2:{typeName:"elaborates", elementColor: '#2d9f9e'},
        3:{typeName:"prerequisite", elementColor: '#9f1e11'},
        4:{typeName:"leads", elementColor: '#264c74'},
        5:{typeName:"similar", elementColor: '#d1bc00'}
    }
});
var refreshJax = function () {
};

function format (fmtstr) {
  var args = Array.prototype.slice.call(arguments, 1);
  return fmtstr.replace(/\{(\d+)\}/g, function (match, index) {
    return args[index];
  });
}

var str = format('{1}, {0}!', 'Hello', 'world');
console.log(str); // prints "world, Hello!"

var delayedJaxLoad = function () {
    var headID = document.getElementsByTagName("head")[0];
    var newScript = document.createElement('script');
    newScript.type = "text/javascript";
    newScript.src = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML";
    newScript.async = true;
    //onload change behavior of mjax refresh
    newScript.onload = function () {
        refreshJax = function () {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        }
    };
    //add the config script
    //TODO: mj displays "(content)" strangly.. need to fix the config. mbe ascii-math is the problem..? AMS ?
    var mJConfigString = "<script type=\"text/x-mathjax-config\">MathJax.Hub.Config({tex2jax: " +
        "{inlineMath: [['$','$'],['$$','$$']]," +
        "skipTags: ['script','noscript','style','textarea','code']," +
        "ignoreClass: 'mjax_ignore'}});</script>";

    $(mJConfigString).appendTo(headID);
    headID.appendChild(newScript);
};

var createNodeDiv = function (id, name, desc, content) {
    var textRep = format("\
    <pre id =\"pre_{0}\"> \
        <b>ID: </b><span class='id_holder'>{0}</span> \
        <br><b>Name: </b><br>  <textarea rows=\"1\" cols=\"30\" id='name_editor_{0}'>{1}</textarea> \
        <br><b>Description: </b><br> <textarea rows=\"1\" cols=\"30\" id='desc_editor_{0}'>{2} </textarea>\
        <br><b>Type: </b><select id='type_editor_{0}'>\
           <option value='default'>Default</option>\
  <option value='audio'>Audio</option>\
  <option value='image'>Image</option>\
  <option value='svg'>SVG</option>\
  <option value='video'>Video</option>\
  <option value='question'>Question</option></select><br><b>Content: </b><br> {3}\
"
        /*
<hr><button class='btn btn btn-default' type='button' data-toggle='collapse' data-target='#collapseExample_{0}' aria-expanded='true' aria-controls='collapseExample_{0}'>\
<span class='glyphicon glyphicon-eye-open' aria-hidden='true'> Edges</span> </button> Add comma separated node id's (temporary or absolute), e.g n0,n2,n5,1568453014 <div class='collapse' id='collapseExample_{0}'> \
<table class='table table-condensed'> \
    <thead> \
      <tr> \
        <th>Edge type</th> \
        <th>Adjacent nodes list</th> \
      </tr> \
    </thead> \
    <tbody> \
      <tr> \
        <td>Answers</td> \
        <td><textarea rows='1' cols='70' id='edge_edit_answer_{0}'></textarea></td> \
      </tr> \
      <tr> \
        <td>Prerequisite</td> \
        <td><textarea rows='1' cols='70' id='edge_edit_prereq_{0}'></textarea></td> \
      </tr> \
      <tr> \
        <td>Leads to</td> \
        <td><textarea rows='1' cols='70' id='edge_edit_leads_{0}'></textarea></td> \
      </tr> \
      <tr> \
        <td>Similar</td> \
        <td><textarea rows='1' cols='70' id='edge_edit_sim_{0}'></textarea></td> \
      </tr> \
      <tr> \
        <td>Elaborates</td> \
        <td><textarea rows='1' cols='70' id='edge_edit_elab_{0}'></textarea></td> \
      </tr> \
    </tbody> \
  </table> \
</div> \
*/
        +
"</pre>\
        ", id, name, desc, content);
    return textRep;
};

window.onload = function () {

    function notifyUIStatus(msg) {
        $("#cy_status").text(msg);
    }

    var createdNodesHolder = [];

    var graphUiState = {
        selected: {isNode: true, id: null},
        mode: UIModeEnum.NEUTRAL,
        edgeToAdd: EdgeTypesEnum.LEADS
    };

    (function bindStateButtonEvents() {
        //states
        $("#cy_btn_dlt").click(function () {
            notifyUIStatus("Mode: deletion");
            graphUiState.mode = UIModeEnum.DELETE;
        });
        $("#cy_btn_look").click(function () {
            notifyUIStatus("Mode: Free hand");
            graphUiState.mode = UIModeEnum.NEUTRAL;
        });
        $("#cy_btn_undo").click(function () {
            notifyUIStatus("Not implemented yet!");
            //not sure how to it yet
        });
        $("#cy_btn_add").click(function () {

            graphUiState.mode = UIModeEnum.ADD_EDGE;
            notifyUIStatus("Mode: add edge");
        });

        //edge types
        $("#cy_btn_gnrc").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.GENERIC;
        });
        $("#cy_btn_ld").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.LEADS;
        });
        $("#cy_btn_ans").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.ANS;
        });
        $("#cy_btn_elb").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.ELAB;
        });
        $("#cy_btn_sm").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.SIMILAR;
        });
        $("#cy_btn_prrq").click(function () {
            notifyUIStatus("changed the edge type as requested");
            graphUiState.edgeToAdd = EdgeTypesEnum.PREREQ;
        });


    })();

    //ACE editor
    var editor = ace.edit("bulk_txt");
    //editor.setTheme("ace/theme/monokai");
    editor.setTheme("ace/theme/twilight");
    editor.getSession().setMode("ace/mode/tex");

    // The graph-data representation in memory.
    var graphData = {};
    var cyLayoutData = {
        layout: {
            name: 'cose',
            padding: 10
        }
    };
    var cyInitData = {
        container: document.getElementById('cy'),
        layout: {
            name: 'cose',
            padding: 10
        },
        style: cytoscape.stylesheet()
            .selector('node')
            .css({
                'shape': 'data(elementShape)',
                'width': 'mapData(weight, 40, 80, 20, 60)',
                'content': 'data(name)',
                'text-valign': 'center',
                'text-outline-width': 2,
                'text-outline-color': 'data(elementColor)',
                'background-color': 'data(elementColor)',
                'color': '#fff'
            })
            .selector(':selected')
            .css({
                'border-width': 3,
                'border-color': '#333'
            })
            .selector('edge')
            .css({
                'opacity': 0.666,
                'width': 'mapData(strength, 70, 100, 2, 6)',
                'content': 'data(name)',
                'text-valign': 'center',
                'font-size': '5px',
                'target-arrow-shape': 'triangle',
                'source-arrow-shape': 'circle',
                'line-color': 'data(elementColor)',
                'source-arrow-color': 'data(elementColor)',
                'target-arrow-color': 'data(elementColor)'
            })
            .selector('edge.questionable')
            .css({
                'line-style': 'dotted',
                'target-arrow-shape': 'diamond'
            })
            .selector('.faded')
            .css({
                'opacity': 0.25,
                'text-opacity': 0
            }),
        ready: function () {
            console.log('cy ready')
        }
    };
    var cyTestElems = {};
    /*{
        elements: {
            nodes: [
                {data: {id: 'j', name: 'Jerry', weight: 65, elementColor: '#6FB1FC', elementShape: 'triangle'}},
                {data: {id: 'e', name: 'Elaine', weight: 45, elementColor: '#EDA1ED', elementShape: 'ellipse'}},
                {data: {id: 'k', name: 'Kramer', weight: 75, elementColor: '#86B342', elementShape: 'octagon'}},
                {data: {id: 'g', name: 'George', weight: 70, elementColor: '#F5A45D', elementShape: 'rectangle'}}
            ],
            edges: [
                {data: {source: 'j', target: 'e', elementColor: '#6FB1FC', strength: 90}},
                {data: {source: 'j', target: 'k', elementColor: '#6FB1FC', strength: 70}},
                {data: {source: 'j', target: 'g', elementColor: '#6FB1FC', strength: 80}},

                {data: {source: 'e', target: 'j', elementColor: '#EDA1ED', strength: 95}},
                {data: {source: 'e', target: 'k', elementColor: '#EDA1ED', strength: 60}, classes: 'questionable'},

                {data: {source: 'k', target: 'j', elementColor: '#86B342', strength: 100}},
                {data: {source: 'k', target: 'e', elementColor: '#86B342', strength: 100}},
                {data: {source: 'k', target: 'g', elementColor: '#86B342', strength: 100}},

                {data: {source: 'g', target: 'j', elementColor: '#F5A45D', strength: 90}}
            ]
        }
    };*/

    //graph
    var cy = cytoscape($.extend({}, cyTestElems, cyInitData));

    //events on cy:
    cy.on('select', 'node', function (e) {
        var wasDeleted = false;
        var selectedId = e.cyTarget.data("id");
        $("#cy_status").text("selected: " + e.cyTarget.data("name") + "(#" + selectedId + ")");
        if (graphUiState.mode == UIModeEnum.ADD_EDGE) {
            if (graphUiState.selected.isNode && (graphUiState.selected.id != null) && (graphUiState.selected.id != selectedId)) {
                console.log(graphUiState.selected.isNode + " prev:" + graphUiState.selected.id + " curr:" + selectedId);
                // add a new edge
                var newEdge = {
                    group: "edges",
                    data: $.extend(
                        {},
                        {
                            id: ID_PREFIX_FOR_CUSTOM_EDGES + edgeCounter,
                            type: "__", // TODO consolidate the types
                            source: graphUiState.selected.id, //previous node
                            target: selectedId, //current node
                            name: "#" + ID_PREFIX_FOR_CUSTOM_EDGES + edgeCounter,
                            strength: 90
                        },
                        EdgeTypesEnum.properties[graphUiState.edgeToAdd])
                };
                console.log(EdgeTypesEnum);
                console.log(newEdge);
                cy.add(newEdge);
                edgeCounter++;
                //cy.layout(cyInitData.layout);


            }
        }
        if (graphUiState.mode == UIModeEnum.DELETE) {
            cy.remove(cy.$("#" + selectedId));
            wasDeleted = true;
        }
        //console.log(e.cyTarget.data("name"));

        //in the end we need to record this selection!
        if (wasDeleted) {
            graphUiState.selected = {isNode: true, id: null};
        } else {
            graphUiState.selected = {isNode: true, id: selectedId};
        }
    });

    cy.on('select', 'edge', function (e) {
        var selectedId = e.cyTarget.data("id");
        $("#cy_status").text("selected: " + e.cyTarget.data("name") + "(#" + selectedId + ")");

        //console.log(e.cyTarget.data("name"));

        if (graphUiState.mode == UIModeEnum.DELETE) {
            cy.remove(cy.$("#" + selectedId));
            graphUiState.selected = {isNode: false, id: null};
            return;
        }

        graphUiState.selected = {isNode: false, id: selectedId};

    });
    //cy.on('unselect', function(e){
    //
    //});
    cy.on('tap', function (event) {
        // cyTarget holds a reference to the originator
        // of the event (core or element)
        var evtTarget = event.cyTarget;

        if (evtTarget === cy) {
            console.log('tap on background');
            graphUiState.selected = {isNode: false, id: null};
        } else {
            console.log('tap on some element');
        }
    });
    // make an element-less graph
    //var cy = cytoscape($.extend({}, {}, cyInitData));


    //load mathjax only after page is ready. TODO: find out why mathjax takes so long to fully load!
    //TODO: select elemnts to parse with mj.
    //setTimeout(delayedJaxLoad,1000);
    delayedJaxLoad();


    $('#parse_button').click(function () {

        console.log("click");

        //var g = new Graph();


        $('#node_dump').empty();
        try {
            var doc = null;
            var path = null;
            //init the graph data
            graphData.elements = {};
            graphData.elements.nodes = [];
            graphData.elements.edges = [];

            var nodeText = editor.getValue();
            var splited = nodeText.split(DELIM);
            console.log(splited);
            //var pathText = $("path_editor").text();
            // todo: create the doc
            doc = {};
            path = {};
            console.log(path);
            var node_counter = 0;

            // split the text and create new divs
            for (var index in splited) {
                //create a new element:
                var newId= "n" + node_counter;
                var txtRep = createNodeDiv(newId, null, null, splited[index]);
                node_counter++;
                var elem = $(txtRep).attr("class", "node_div").appendTo('#node_dump');

                createdNodesHolder.push(elem)



            } // end for
            // created all the nodes. now reset mathjax
            refreshJax();

            //now connect the edges
            //the data schema is node.adj_list.edge_type.[edges..]
            //TODO: encapsulate the schema in some object/function so we have only 1 project-wide yaml->json parser

            //append node to graph
               /* graphData.elements.nodes.push({
                    data: {
                        id: node,
                        name: doc[node][NAME_KEY] + " (#" + node + ")",
                        weight: 65,
                        elementColor: '#6FB1FC',
                        elementShape: 'triangle'
                    }
                })
            var counter = 0;
            for (var nodeKey in doc) {
                //console.log("yo");
                if (doc.hasOwnProperty(nodeKey)) {
                    for (var edgeTypeKey in doc[nodeKey][ADJACENCY_LIST_KEY]) {
                        for (var adjKey in doc[nodeKey][ADJACENCY_LIST_KEY][edgeTypeKey]) {
                            targetNodeName = doc[nodeKey][ADJACENCY_LIST_KEY][edgeTypeKey][adjKey];
                            //console.log(nodeKey + " : " + edgeTypeKey + " " + targetNodeName);

                            graphData.elements.edges.push({
                                data: {
                                    source: nodeKey,
                                    type: edgeTypeKey,
                                    id: "e" + counter,
                                    name: "#e" + counter + "(." + edgeTypeKey + ")",
                                    target: targetNodeName,
                                    elementColor: '#6FB1FC',
                                    strength: 90
                                }
                            });
                            counter++;
                            //g.addEdge(nodeKey, targetNodeName, {directed: true, label: edgeTypeKey});
                        }
                    }
                }
            }*/ //end for


            // var pretty = JSON.stringify(graphData, null, 4);

            cy.add(graphData.elements);
            console.log(cy.json());
            cy.layout(cyInitData.layout);

        } //end try
        catch (e) {
            alert(e);
        }

    });


    function inferColor(str){
        // temp implementation!
        colors = ["#6FB1FC", "#EDA1ED", "#86B342", "#F5A45D"];
        return colors[Math.floor(Math.random() * colors.length)];
    }
    function inferShape(str){
        // temp implementation!
        var shapes = ["triangle", "ellipse", "octagon", "rectangle"];
        return shapes[Math.floor(Math.random() * shapes.length)];
    }

    $('#graph_button').click(function () {
        for (elemIndex in createdNodesHolder) {
            var currNode = createdNodesHolder[elemIndex];
            var typeElem = currNode.find("[id*='type_']"),
                idElem = currNode.find("[class*='id_holder']"),
                nameElem = currNode.find("[id*='name_']"),
                descElem = currNode.find("[id*='desc_']");

            var  nodeType = typeElem.val();
            var newNode = {
                data: {
                    id: idElem.text(),
                    name: nameElem.val(),
                    desc: descElem.val(),
                    type: nodeType,
                    weight: 65, // todo: thats bullshit
                    elementColor: inferColor(nodeType),
                    elementShape: inferShape(nodeType),
                    position: { x: 20*elemIndex, y: 10*elemIndex }, // todo: idk how position works exactly.. does in iterfere with layouting?
                    renderedPosition: { x: 20*elemIndex, y: 10*elemIndex }
                }

            };
            graphData.elements.nodes.push(newNode);

            //console.log(newNode);
        } // endfor
        var nodes = cy.nodes();
        console.log("nodes:");
        console.log(nodes);
        for (nodeIndex in nodes){


            if($.isNumeric(nodeIndex)) { // todo: Quite the hack! we want a better way to know if the index is good
                var currElem = nodes[nodeIndex];
            // other than if it numeric!
                console.log(format("index:{0} holdning: {1}",nodeIndex, currElem.data("id")));
                var pos = {x:Math.floor(Math.random() * 100), y:100};
                currElem.renderedPosition( pos );
            }
        }

        cy.add(graphData.elements);
        //cy.layout(cyInitData.layout);
        options = {
            name: 'random',
            padding: 10,
            fit: true
        };
        cy.layout( options );
    });
    $('#send').click(function () {
        //send to server! detailed
        // todo: add the node content AND figure out a "pathlet" on the client and send it too
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "../bulk",
            data: JSON.stringify(graphData),
            success: function (data) {
                console.log(data.text);
//                        console.log(data.article);
            },
            dataType: "json"
        });
    });

};



